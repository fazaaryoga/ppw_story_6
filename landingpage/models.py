from django.db import models

# Create your models here.
class Status(models.Model):
    post_time = models.DateTimeField(auto_now_add=True)
    status = models.TextField()