from django.test import Client, TestCase
from .views import index
from .models import Status
from .apps import *
from .forms import Status_form
from django.urls import resolve

# Create your tests here.

class Tests(TestCase):
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_create_status(self):
        new_status = Status.objects.create(status = 'hello')
        self.assertTrue(isinstance(new_status, Status))
        self.assertTrue(new_status.status, 'hello')
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_status_post(self):
        status = 'hi'
        post_data = {'status': 'status'}
        post = Client().post('/', post_data)
        self.assertEqual(post.status_code, 200)

    def test_app(self):
        self.assertEqual(LandingpageConfig.name, 'landingpage')

    def test_valid_post(self):
        form = Status_form({'status': 'hello'})
        self.assertTrue(form.is_valid())
    
    def test_invalid_post(self):
        form = Status_form({'status':''})