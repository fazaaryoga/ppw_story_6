from django import forms

class Status_form(forms.Form):
    statusInput = {'id': 'forms', 'name' : 'status'}

    status = forms.CharField(label='Status',required=True, initial="Say how you feel", widget=forms.Textarea(statusInput))
