from django.shortcuts import render
from .forms import Status_form
from .models import Status

# Create your views here.

def index(request):
    status_list = Status.objects.all()
    if request.method == 'POST':
        blank_form = Status_form()
        filled_form = Status_form(request.POST)
        if filled_form.is_valid():
            status = Status(status = filled_form.data['status'])
            status.save()
            form = {'form':blank_form, 'status_list':status_list}
            return render(request, 'index.html', form)

    else:
        blank_form = Status_form()
        form = {'form':blank_form, 'status_list':status_list}
        return render(request, 'index.html', form)

    